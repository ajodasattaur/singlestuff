from troposphere import (
    Template,
    Parameter,
    Join,
    Ref,
    ImportValue,
    Output,
    Export
)

from troposphere.wafregional import (
    Rule,
    SqlInjectionMatchSet,
    WebACL,
    SizeConstraintSet,
    IPSet,
    XssMatchSet,
    Predicates,
    SqlInjectionMatchTuples,
    FieldToMatch,
    Action,
    Rules,
    SizeConstraint,
    XssMatchTuple,
    WebACLAssociation
)
from cfn_flip import to_yaml

# Constants
APP_NAME = 'looker'

STAGE = 'dev'

BYTE_LIMIT = '8192'

COMPONENT = 'waf'


# Create empty template
template = Template()


# Begin parameters
template.add_parameter(Parameter(
    "Stage",
    Type = "String",
    Description = "Stage of the template",
    Default = "dev"
))


# Begin MatchSets (Conditions in the UI)
XssMatchSet = template.add_resource(XssMatchSet(
    "XssMatchSet",
    Name= "{}-{}-xssmatch".format(STAGE, APP_NAME),
    XssMatchTuples=[
        XssMatchTuple(
            FieldToMatch=FieldToMatch(
                Type="QUERY_STRING",
            ),
            TextTransformation="URL_DECODE"
        ),
        XssMatchTuple(
            FieldToMatch=FieldToMatch(
                Type="QUERY_STRING",
            ),
            TextTransformation="HTML_ENTITY_DECODE"
        ),
        XssMatchTuple(
            FieldToMatch=FieldToMatch(
                Type="BODY",
            ),
            TextTransformation="URL_DECODE"
        ),
        XssMatchTuple(
            FieldToMatch=FieldToMatch(
                Type="BODY",
            ),
            TextTransformation="HTML_ENTITY_DECODE"
        ),
        XssMatchTuple(
            FieldToMatch=FieldToMatch(
                Type="URI",
            ),
            TextTransformation="URL_DECODE"
        )
    ]
))


# Block SQL injections 
template.add_resource(SqlInjectionMatchSet(
    "SqlInjectMatchSet",
    Name = "{}-{}-sqlmatch".format(STAGE, APP_NAME),
    SqlInjectionMatchTuples = [
        SqlInjectionMatchTuples(
            FieldToMatch = FieldToMatch(
                Type = "QUERY_STRING"
            ),
            TextTransformation = "URL_DECODE"
        ),
        SqlInjectionMatchTuples(
            FieldToMatch = FieldToMatch(
                Type = "QUERY_STRING"
            ),
            TextTransformation = "HTML_ENTITY_DECODE"
        ),
        SqlInjectionMatchTuples(
            FieldToMatch = FieldToMatch(
                Type = "URI"
            ),
            TextTransformation = "URL_DECODE"
        )
    ] 
))


# Limits requests to 8192 bytes
template.add_resource(SizeConstraintSet(
    "SizeMatchSet",
    Name = "{}-{}-sizematch".format(STAGE, APP_NAME) ,
    SizeConstraints = [
        SizeConstraint(
            ComparisonOperator = "GT",
            TextTransformation = "NONE",
            FieldToMatch = FieldToMatch(
                Type = "BODY"
            ),
            Size = "8192"
        )
    ]
))


# Begin rules

# Block SQL Injects
template.add_resource(Rule(
    "SqlInjectRule",
    Predicates = [
        Predicates(
            DataId = Ref("SqlInjectMatchSet"),
            Type = "SqlInjectionMatch",
            Negated = False
        )
    ],
    Name = "{}-{}-sqlinject-rule".format(STAGE, APP_NAME),
    MetricName = "{}{}sqlinjectrule".format(STAGE, APP_NAME)

)) 

# Block XSS attacks
template.add_resource(Rule(
    "XSSRule",
    Name = "{}-{}-xss-rule".format(STAGE, APP_NAME),
    Predicates = [
        Predicates(
            DataId = Ref("XssMatchSet"),
            Type = "XssMatch",
            Negated = False
        )
    ],
    MetricName = "{}{}xssrule".format(STAGE, APP_NAME)
))

# Block large requests
template.add_resource(Rule(
    "SizeMatchRule",
    Name = "{}-{}-sizematch-rule".format(STAGE, APP_NAME),
    MetricName = "{}{}sizematchrule".format(STAGE, APP_NAME),
    Predicates = [
        Predicates(
            DataId = Ref("SizeMatchSet"),
            Type = "SizeConstraint",
            Negated = False
        )
    ],
))


# Create the ACL
template.add_resource(WebACL(
    "DevLookerWACL",
    Name = "{}-{}-{}".format(STAGE, APP_NAME, COMPONENT),
    DefaultAction = Action(
        Type = "ALLOW"
    ),
    Rules = [
        Rules(
            Action = Action(
                Type = "BLOCK"
            ),
            Priority = 1,
            RuleId = Ref("XSSRule")
        ),
        Rules(
            Action = Action(
                Type = "BLOCK"
            ),
            Priority = 2,
            RuleId = Ref("SqlInjectRule")
        ),
        Rules(
            Action = Action(
                Type = "BLOCK"
            ),
            Priority = 3,
            RuleId = Ref("SizeMatchRule")
        )
    ],
    MetricName = "{}{}{}".format(STAGE, APP_NAME, COMPONENT)
))

# Associate with the dev-looker api ELB
template.add_resource(WebACLAssociation(
    "DevLookerWACLAssociation",
    ResourceArn = ImportValue("dev-looker-api-loadbalancer-arn"),
    WebACLId = Ref("DevLookerWACL")
))


# Begin outputs
template.add_output(Output(
    "XSSRuleID",
    Value = Ref("XSSRule"),
    Export=Export(Join("", ["{}".format(STAGE), "-looker-waf-xss-rule-id"]))
))

template.add_output(Output(
    "SizeMatchRuleID",
    Value = Ref("SizeMatchRule"),
    Export=Export(Join("", ["{}".format(STAGE), "-looker-waf-sizematch-rule-id"]))
))

template.add_output(Output(
    "SQLInjectRuleID",
    Value = Ref("SqlInjectRule"),
    Export=Export(Join("", ["{}".format(STAGE), "-looker-waf-sqlinject-rule-id"]))
))

print(template.to_yaml(clean_up = True))