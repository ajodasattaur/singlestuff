from troposphere import (
    ec2,
    Ref,
    Sub,
    Tags,
    Template,
    GetAtt
)

# Create empty template
template = Template()

# Begin constants
STAGE = "dev"

APP_NAME = "ajodatest"

# CIDR for the VPC
CIDR_BLOCK = "10.0.0.0/16"

# Tuple of public subnets
PUBLIC_SUBNETS = [

    ("PublicSubnet01", "USEast1A", "us-east-1a", "10.0.0.0/24"),
    ("PublicSubnet02", "USEast1B", "us-east-1b", "10.0.1.0/24"),
    ("PublicSubnet03", "USEast1C", "us-east-1c", "10.0.2.0/24"),

]

# Tuple of private subnets
PRIVATE_SUBNETS = [

    ("PrivateSubnet01", "USEast1A", "us-east-1a", "10.0.3.0/24"),
    ("PrivateSubnet02", "USEast1B", "us-east-1b", "10.0.4.0/24"),
    ("PrivateSubnet03", "USEast1C", "us-east-1c", "10.0.5.0/24"),

]


# When set to False, will only create 1 NAT Gateway and route all private subnets to it
# True creates 1 NAT Gateway per private subnet
NAT_GATEWAY_HA = True


template.add_description(
    "Template for creating a VPC with public/private subnets"
)


# Create the VPC
template.add_resource(ec2.VPC(
    "%sVPC" % (APP_NAME),
    CidrBlock = "%s" % (CIDR_BLOCK),
    Tags = Tags(
        Name = "%s-%s-vpc" % (APP_NAME, STAGE),
        Stage = "%s" % (STAGE),
        Component = "vpc"
    )
))


# Create all public subnets
for PUBLIC_SUBNET in PUBLIC_SUBNETS:
    template.add_resource(ec2.Subnet(
        "{}{}".format(APP_NAME, PUBLIC_SUBNET[0]),
        AvailabilityZone = "%s" % (PUBLIC_SUBNET[2]),
        CidrBlock = "%s" % (PUBLIC_SUBNET[3]),
        VpcId = Ref("%sVPC" % APP_NAME)        
    ))


# Create all private subnets
for PRIVATE_SUBNET in PRIVATE_SUBNETS:
    template.add_resource(ec2.Subnet(
        "{}{}".format(APP_NAME, PRIVATE_SUBNET[0]),
        AvailabilityZone = "%s" % (PRIVATE_SUBNET[2]),
        CidrBlock = "%s" % (PRIVATE_SUBNET[3]),
        VpcId = Ref("%sVPC" % APP_NAME)
    ))
    
    if NAT_GATEWAY_HA:

        # Provision an EIP per private subnet if HA_NAT is True
        template.add_resource(ec2.EIP(
            "PrivateNATEIP{}".format(PRIVATE_SUBNET[0]),
            Domain = 'vpc'
        ))

        # Provision a NAT Gateway per private subnet using the EIP provisioned before
        template.add_resource(ec2.NatGateway(
            "PrivateNATGateway{}".format(PRIVATE_SUBNET[0]),
            AllocationId = GetAtt("PrivateNATEIP{}".format(PRIVATE_SUBNET[0]), "AllocationId"),
            SubnetId = Ref("{}{}".format(APP_NAME, PRIVATE_SUBNET[0]))
        ))
        

# End resources

# Begin outputs

print(template.to_yaml())