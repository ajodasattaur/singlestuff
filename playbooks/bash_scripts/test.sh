#!/bin/bash

if [[ -e .env ]]; then
        echo 'testing'
fi

# Function that reads the currently executing function and takes and argument 
# Must be used inside another function otherwise exception Bad Subsittution
loginfo() {
  echo "${FUNCNAME[ 1 ]} : $1"
}

vaildate_syntax() {
    loginfo 'testing syntax'
    /bin/bash -x placeholder.sh
    RESULT=$?
    if [[ $RESULT != 0 ]]; then
      echo "Syntax failed. Error code: $RESULT"
    fi
}

vaildate_syntax