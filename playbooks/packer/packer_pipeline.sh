#!/bin/bash
if [ -e .env ]; then
	echo 'global env found, loading'
	export $(xargs < .env)
fi

ACTION=${1:?'ERROR: Please specify an action (actions supported: build)'}
TYPE=${2:?'ERROR: Please specify a build type (types supported: packer, vagrant)'}
TAG=${3:?'ERROR: Please specify a target image'}

test_packer_config() {
  echo 'running packer validate'
  packer validate "${TAG}-packer.json"
  RESULT=$?

  if [[ $RESULT != 0 ]]; then
    echo 'packer validate failed'
    return $RESULT
  else
    return 0
  fi
}

build_packer_config() {
  echo 'running packer build'
  packer build "${TAG}-packer.json"
  RESULT=$?

  if [[ $RESULT != 0 ]]; then
    echo 'packer build failed'
    return $RESULT
  else
    return 0
  fi
}

build_packer() {
  echo 'testing packer configuration'
  test_packer_config
  RESULT=$?

  if [[ $RESULT != 0 ]]; then
    echo 'packer configuration failed to validate'

    return 1
  fi

  echo 'building packer image'
  build_packer_config
  RESULT=$?

  if [[ $RESULT != 0 ]]; then
    echo 'packer configuration failed to build'

    return 1
  fi

  echo 'packer build complete'
  return 0
}

build() {
  cd "./${TAG}"

  if [ -e .env ]; then
    echo 'tag specific env found, loading'
    export $(xargs < .env)
  fi

  if [[ $TYPE == "packer" ]]; then
    echo 'building packer'
    build_packer
    RESULT=$?
  fi  

  if [[ $RESULT != 0 ]]; then
    echo 'build failed'
    return 1
  fi
}

$ACTION
RESULT=$?

if [[ $RESULT != 0 ]]; then
  echo 'pipeline failed'
  exit $RESULT
fi