from packerlicious import (
    builder,
    provisioner,
    Template
)

# create empty template
template = Template()


# Begin constants

DESCRIPTION = 'Current freeswitch ami'

INSTANCE_TYPE = 'c4.large'

REGION = 'us-east-1'

SOURCE_AMI = 'ami-b6dc26cc'

AMI_NAME = 'freeswitch-gen3'

ACCOUNT_ID = '740862182994'

template.add_builder(
    builder.AmazonEbs(
        access_key = '',
        secret_key = '',
        profile = '740',
        region = '%s' % (REGION),
        instance_type = '%s' % (INSTANCE_TYPE),
        ami_description = '%s' % (DESCRIPTION),
        source_ami = '%s' % (SOURCE_AMI),
        ami_name = '%s' % (AMI_NAME),
        ssh_username = 'admin',
        vpc_id = 'vpc-c15981a7',
        associate_public_ip_address = True,
        subnet_id = 'subnet-ab632ae2'
    )
)

template.add_provisioner(
        provisioner.Shell(
            script="setup_things.sh"
        )
    )

print(template.to_json())