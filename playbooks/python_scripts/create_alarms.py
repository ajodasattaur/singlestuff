import yaml
from troposphere import (
    Ref,
    Sub,
    Template,
    cloudwatch
)

# create empty template
template = Template()

# begin constants

CLUSTER_NAME = 'ajodaTest'
CLUSTER_SIZE = 4
DNS_ZONE = ".singlecomm.com"


# define SNS Topics
OPS_GENIE_ARN = "arn:aws:sns:us-east-1:740862182994:Singlecomm_Ops_Genie74"
OPS_WARNING_ARN = "arn:aws:sns:us-east-1:740862182994:Ops_Slack_Warning"


# Load alarms into dict from yaml
with open('alarms.yml', 'r') as alarms:
    for key, value in yaml.load(alarms).items():
        for instances in range(1, CLUSTER_SIZE + 1):
            RESOURCE_NAME = '{}FS{}Alarm{}'.format(CLUSTER_NAME, str(instances).zfill(2), value['Alarm'])
            DNS_NAME = '{}-fs{}{}'.format(CLUSTER_NAME, str(instances).zfill(2), DNS_ZONE)
            TRUNCATED_NAME = "{}-fs{}".format(CLUSTER_NAME, str(instances).zfill(2))

            template.add_resource(cloudwatch.Alarm(
                "{}".format(RESOURCE_NAME),
                Namespace = '%s' % (value['Namespace']),
                Statistic = '%s' % (value['Statistic']),
                Period = '%s' % (value['Period']),
                Threshold = '%s' % (value['Threshold']),
                ComparisonOperator = '%s' % (value['ComparisonOperator']),
                EvaluationPeriods = '%s' % (value['EvaluationPeriods']),
                MetricName = '%s' % (value['MetricName']),
                Dimensions = [
                    cloudwatch.MetricDimension(
                        Name = 'FreeSWITCH Node',
                        Value = '%s' % (TRUNCATED_NAME)
                    )
                ]
            ))
print(template.to_yaml())