# usage: python untagged_snapshots.py --profile [655, 740]

import argparse, boto3, datetime

def main():

    snapshot_ids = []

    # Create dict of that contains information about ALL snapshots in the account
    snapshots = [s for s in ec2_client.describe_snapshots(
        OwnerIds=[
            ('self')
        ]
    )['Snapshots']]

    # Append to the empty lists (snapshot_ids, start_times) when there's an instance without tags
    for snap in snapshots:
        try:
            tagged = snap['Tags']

        except KeyError:
            
            snapshot_ids.append(snap['SnapshotId'])
    
    print(len(snapshot_ids))

if __name__ == '__main__':

    # Defining which AWS account to run against
    parser = argparse.ArgumentParser(description = "Gets snapshot ID's that have no tags")
    parser.add_argument("-p", "--profile", nargs = "?", default = str(655), help = "AWS account to target")
    args = parser.parse_args()

    if args.profile == "655":
        account_id = "655772981054"
    elif args.profile == "740":
        account_id = "740862182994"

    session = boto3.Session(profile_name = str(args.profile))
    ec2_client = session.client('ec2')
    print('Connected to the {} account'.format(account_id))
    exit(main())