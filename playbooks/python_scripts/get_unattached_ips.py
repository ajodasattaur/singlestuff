import argparse, boto3

def main():

    unallocated_ips = []
    ips = [s for s in ec2_client.describe_addresses()['Addresses']]

    for ip in ips:
        print(ip['PublicIp'])

if __name__ == '__main__':

    # Defining which AWS account to run against
    parser = argparse.ArgumentParser(description = "Gets snapshot ID's that have no tags")
    parser.add_argument("-p", "--profile", nargs = "?", default = str(655), help = "AWS account to target")
    args = parser.parse_args()

    if args.profile == "655":
        account_id = "655772981054"
    elif args.profile == "740":
        account_id = "740862182994"

    session = boto3.Session(profile_name = str(args.profile))
    ec2_client = session.client('ec2')
    print('Connected to the {} account'.format(account_id))
    exit(main())