import slackclient, os

SLACK_TOKEN = os.environ.get('SLACK_TOKEN')

slack_client = slackclient.SlackClient(SLACK_TOKEN)


def channels_list():
    channels_call = slack_client.api_call("channels.list")
    if channels_call.get("ok"):
        return channels_call['channels']

if __name__ == "__main__":

    channels = channels_list()

    if channels:
        print("Channels: ")
        for channel in channels:
            print("Channel name: {} Channel id: {}").format(channel['name'], channel['id'])
            
    else:
        print('Unable to authenticate')

