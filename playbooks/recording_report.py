from hurry.filesize import size
import operator
import boto
import datetime

def chunks(l, n):
    """Yield successive n-sized chunks from l."""
    for i in xrange(0, len(l), n):
        yield l[i:i+n]

names = ('nexrep', 'argo', 'beachbody', 'honk', 'liveops', 'lt', 'forcefactor', 'pil', 'synergixx', 'umg')
for n in names:
        bucketName = n+"-c3s-recordings"
        s3 = boto.connect_s3(profile_name='655')
        bucket = s3.get_bucket(bucketName)
        print "Connected to bucket "+bucketName

        all_keys = [(v.name, v.version_id, v.size) for v in bucket.list()]
        print "Found {} keys.".format(len(all_keys))

        ## Number of years into past, e.g. 2 = 2017 + 2016
        nYears=2
        callData = [[[0,0] for i in range(12)] for i in range(nYears)]
        timeNow = datetime.datetime.now()
        for keys in chunks(all_keys, 1000):
                for key in keys:
                        ## Get date, 0 is name
                        date = key[0].split("/")
                        objTime = datetime.datetime.strptime(date[0], "%Y-%m-%d")
                        diff  = abs((timeNow - objTime).days)

                        ## Older than 1 year = add size to running total
                        if diff > 365:
                                if (timeNow.year - objTime.year) > nYears:
                                        continue;

                                ## Increment call count, and size.
                                callData[timeNow.year-1-objTime.year][objTime.month-1][0] += 1
                                callData[timeNow.year-1-objTime.year][objTime.month-1][1] += key[2]

        ## Print out report, aggregate GB size/month
        months = ("Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec")
        print "Sizes are 1024 = 1 KB = K"
        print "CLUSTER: "+n
        for i,y in enumerate(callData):
                print "----------------------------------------"
                for j,m in enumerate(y):
                        print "{:4d} {:3s}: {:<6d} calls of size {:<7s}. Total $$: {:<10f}".format(timeNow.year-1-i, months[j], m[0], size(m[1]), 0.023/(1*10**9)*m[1])