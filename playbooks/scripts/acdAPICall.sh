#!/bin/bash
client_name=$1
feedString=$2
callDispo=$3

call_ID=(
"1986879"
)

for id in "${call_ID[@]}"; do
  curl https://$client_name/feed/generic/$feedString/?call_id=$id&call_dispo=$callDispo
done
echo $1 $2 $3
