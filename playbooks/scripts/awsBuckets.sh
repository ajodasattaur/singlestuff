#!/bin/bash


buckets=(
"argo-c3s-backups"
"umg-c3s-backups"
"cuore-c3s-backups"
"lt-c3s-backups"
"synergixx-c3s-backups"

)

for name in "${buckets[@]}"; do
  aws s3api delete-object --bucket singlecomm-cf --key $name/
done
