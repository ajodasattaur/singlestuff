#!/bin/bash

fsIPs=$(cat ../bwIPs.txt)


for line in $fsIPs
  do
    aws ec2 describe-addresses --public-ips "$line" --query 'Addresses[*].[InstanceId]' --output text
  done

