from troposphere import (
    cloudwatch,
    Ref,
    Sub,
    Template,
)

# Create empyt template
template = Template()

# Define SNS Topics
OPS_GENIE_ARN = "arn:aws:sns:us-east-1:740862182994:Singlecomm_Ops_Genie74"
OPS_WARNING_ARN = "arn:aws:sns:us-east-1:740862182994:Ops_Slack_Warning"

# Define cluster parameters
CLUSTER_SIZE = 1
CLUSTER_NAME = "argo"
DNS_ZONE = ".singlecomm.com"

# Define the alarms
FREESWITCH_ALARMS = [

    {
        'Alarm': '01',
        'AlarmName': 'test',
        'ComparisonOperator': 'GreaterThanThreshold',
        'Statistic': 'Average',
        'TreatMissing': 'missing',
        'Namespace': 'AWS/Route53',
        'Threshold': '1',
        'Period': '60',
        'EvaluationPeriods': 1

    }
]

# Create alarms for each instance. Starting at 1 because.....
for instances in range(1, CLUSTER_SIZE + 1):
    
    # Create DNS names for the alarms
    DNS_NAME = "{}-fs{}{}".format(CLUSTER_NAME, str(instances).zfill(2), DNS_ZONE)
    TRUNCATED_NAME = "{}{}".format(CLUSTER_NAME, str(instances).zfill(2))

    for alarms in FREESWITCH_ALARMS:

        template.add_resource(cloudwatch.Alarm(
            "{}Alarm{}".format(TRUNCATED_NAME, alarms['Alarm']),
            Namespace = '%s' % (alarms['Namespace']),
            Statistic = '%s' % (alarms['Statistic']),
            Period = '%s' % (alarms['Period']),
            Threshold = '%s' % (alarms['Threshold']),
            ComparisonOperator = '%s' % (alarms['ComparisonOperator']),
            EvaluationPeriods = '%s' % (alarms['EvaluationPeriods']),
            MetricName = 'ActiveChannels',
            Dimensions = [
                cloudwatch.MetricDimension(
                    Name = 'FreeSWITCH Node',
                    Value = '%s' % (TRUNCATED_NAME)
                )
            ]         
        ))

print(template.to_yaml())