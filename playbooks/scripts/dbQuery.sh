#!/bin/bash

results = $(
cd /var/www/devops && sudo php query.php
 'SELECT
callUserId,
personFirstName,
personLastName,
COUNT(callId) AS calls,
SUM( IF( cdrRTPInMediaPacketCount = 0 , 1 , 0 ) ) AS noMediaCalls,
SUM( IF( cdrRTPInFlushPct > 1 , 1 , 0 ) ) AS rtpFlushOver1Pct,
ROUND( AVG( cdrRTPInFlushPct ) , 4 ) AS avgRtpFlush
FROM
CDR
JOIN `Call` ON cdrCallId=callId AND callType='A'
JOIN `User` ON callUserId=userId
JOIN `Person` ON personId=userPersonId
WHERE
cdrCreatedON BETWEEN '##_1 00:00:00' AND '##_2 23:59:59'
GROUP BY callUserId
ORDER BY avgRtpFlush DESC , $_SDATE , $_DATE );'
)

echo $results
