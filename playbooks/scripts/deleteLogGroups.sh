#!/bin/bash

CLIENTID=$1

logGroups=$(aws logs describe-log-groups --log-group-name-prefix $CLIENTID | grep "logGroupName" | awk '{print $2}' | sed -s 's/,/ /g' | tr -d '"')

for logs in $logGroups
do
		aws logs delete-log-group --log-group-name $logs
done


