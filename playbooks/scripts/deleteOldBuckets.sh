#!/bin/bash

echo ""
echo "Please enter the cluster: "

read cluster

rawBuckets=$(aws s3api list-buckets | grep $cluster | awk '{print $2}' | tr -d '"')
if [ -z $rawBuckets ];
then
		echo "No buckets found"
		echo ""
   		exit 1
fi
		echo ""
		echo -e "The following buckets will be deleted: 
		$rawBuckets
		"
		echo "Do you want to delete those buckets? (yes/no): "

read answer
case $answer in

Yes|yes)

for buckets in $rawBuckets
do
		aws s3 rb s3://$buckets --force
done
;;

No|no)
		echo ""
		echo "Doing nothing to the buckets"
;;

esac

