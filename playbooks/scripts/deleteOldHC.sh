#!/bin/bash

help () {
echo "ONLY TO BE USED ON DELETED CLUSTERS"
echo -e "This script will go out and get the health check IDs of the cluster you specify"
}


if [ "$1" == "--help" ]; 
then
		help
		exit 1
fi


echo ""
echo "Please enter the name of the cluster: "
echo ""

read cluster

rawHC=$(aws route53 list-health-checks | grep "$cluster" --after-context 2 | grep "Id" | awk '{print $2}' | tr -d '"')
if [ -z $rawHC ]; 
then
		echo "Didnt find any health checks for $cluster"
		exit 1
fi

echo ""
echo "The following HCs will be deleted
$rawHC
"
echo ""
echo "Do you want to delete these HCs? (yes/no): "

read choice
case $choice in

Yes|yes)

for HC in $rawHC
do
		aws route53 delete-health-check --health-check-id $HC
done
;;

No|no)

		echo ""
		echo "Doing nothing to HC's"
		exit 1
;;
esac
