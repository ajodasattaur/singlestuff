#!/bin/bash

echo ""
echo "Please enter the name of the cluster: "
echo ""

read cluster

rawInstances=$(aws rds describe-db-instances --query "DBInstances[].DBInstanceIdentifier[]" | grep "liveops" | sed 's/,/ /g' | tr -d '"' | grep $cluster)

if [ -z $rawInstances ]; then
   echo "No instances found."
   exit 1
fi


echo ""
echo "The following instances were found: 
$rawInstances"
echo ""
echo "Please confirm deletion (yes/no): "

read answer

case $answer in

Yes|yes)

for dbinstances in $rawInstances

do
    aws rds delete-db-instance --db-instance-identifier $dbinstances --final-db-snapshot-identifier $dbinstances-final-snapshot
done
;;

No|no)

echo ""
echo "Exiting script"
exit 1
;;

esac
