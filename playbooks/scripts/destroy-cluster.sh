#!/bin/bash
# Cleanup script for old clusters

CLUSTER_ID=$1
REGION=us-east-1

# Need to add some double checking code here to prevent the user from screwing themselves over

# ==== EC2 removal ====
#aws ec2 terminate-instances --instance-ids ${CLUSTER_ID}-001.sngl-cm ${CLUSTER_ID}-002.sngl-cm --region $REGION
aws ec2 delete-key-pair --key-name ${CLUSTER_ID}-sngl-cm --region $REGION

# ==== Route53 ====

# ==== AWS log cleanup ==== 
#- Section Complete
# This command only outputs if there is an error, which we don't care about

echo -n "Removing log groups"
aws logs delete-log-group --log-group-name $CLUSTER_ID".sngl.cm/auth.log" --region $REGION && echo -n "."
aws logs delete-log-group --log-group-name $CLUSTER_ID".sngl.cm/dmesg" --region $REGION && echo -n "."
aws logs delete-log-group --log-group-name $CLUSTER_ID".sngl.cm/freeswitch.log" --region $REGION && echo -n "."
aws logs delete-log-group --log-group-name $CLUSTER_ID".sngl.cm/kern.log" --region $REGION && echo -n "."
aws logs delete-log-group --log-group-name $CLUSTER_ID".sngl.cm/manager-commflow.log" --region $REGION && echo -n "."
aws logs delete-log-group --log-group-name $CLUSTER_ID".sngl.cm/manager-data.log" --region $REGION && echo -n "."
aws logs delete-log-group --log-group-name $CLUSTER_ID".sngl.cm/manager-event.log" --region $REGION && echo -n "."
aws logs delete-log-group --log-group-name $CLUSTER_ID".sngl.cm/manager-export.log" --region $REGION && echo -n "."
aws logs delete-log-group --log-group-name $CLUSTER_ID".sngl.cm/manager-report.log" --region $REGION && echo -n "."
aws logs delete-log-group --log-group-name $CLUSTER_ID".sngl.cm/manager-media.log" --region $REGION && echo -n "."
aws logs delete-log-group --log-group-name $CLUSTER_ID".sngl.cm/manager-metric.log" --region $REGION && echo -n "."
aws logs delete-log-group --log-group-name $CLUSTER_ID".sngl.cm/manager-rubbish.log" --region $REGION && echo -n "."
aws logs delete-log-group --log-group-name $CLUSTER_ID".sngl.cm/manager-s3.log" --region $REGION && echo -n "."
aws logs delete-log-group --log-group-name $CLUSTER_ID".sngl.cm/nginx.error.log" --region $REGION && echo -n "."
aws logs delete-log-group --log-group-name $CLUSTER_ID".sngl.cm/nginx.access.log" --region $REGION && echo -n "."
aws logs delete-log-group --log-group-name $CLUSTER_ID".sngl.cm/production.log" --region $REGION && echo -n "."
aws logs delete-log-group --log-group-name $CLUSTER_ID".sngl.cm/provider-cluster.log" --region $REGION && echo -n "."
aws logs delete-log-group --log-group-name $CLUSTER_ID".sngl.cm/provider-distributor.log" --region $REGION && echo -n "."
aws logs delete-log-group --log-group-name $CLUSTER_ID".sngl.cm/provider-integrations.log" --region $REGION && echo -n "."
aws logs delete-log-group --log-group-name $CLUSTER_ID".sngl.cm/provider-telco.log" --region $REGION && echo -n "."
aws logs delete-log-group --log-group-name $CLUSTER_ID".sngl.cm/rkhunter.log" --region $REGION && echo -n "."
echo "Done!"

# Remove the Alarms

# Delete the S3 buckets
# issue here is the versioning. If it's enabled the bucket wont delete

aws s3 rb s3://cdn-$CLUSTER_ID-sngl-cm --force --region $REGION
aws s3 rb s3://$CLUSTER_ID-singlecomm-net --force --region $REGION
aws s3 rb s3://$CLUSTER_ID-singlecomm-net-or --force --region $REGION

#Delete the RDS DB
#there is an option for --no-skip-final-snapshot if necessary

aws rds delete-db-instance --db-instance-identifier $CLUSTER_ID-sngl-cm --skip-final-snapshot --region $REGION

# !!! Need to wait for the delete DB to finish !!!

aws rds delete-db-subnet-group --db-subnet-group-name $CLUSTER_ID-sngl-cm --region $REGION


# Delete the Elasticache Replication Group

aws elasticache delete-replication-group --replication-group-id $CLUSTER_ID-sngl-cm --region $REGION
aws elasticache delete-cache-subnet-group --cache-subnet-group-name $CLUSTER_ID-sngl-cm --region $REGION

# Delete the VPC - This has to be the last thing we do.

#aws ec2 delete-vpc --vpc-id vpc-######

# Remove IAM User

# Remove IAM Group

# Remove Cloudfront Setup

# Remove Cloudfront's referenced S3 setup

