#!/bin/bash

REGION=us-east-1
CLUSTERID=$1
DELETE=$2

#Gather AWS Log groups

logGroups=$(aws logs describe-log-groups --log-group-name-prefix $CLUSTERID | grep "logGroupName" | awk '{print $2}' | sed -s 's/,/ /g' | tr -d '"')

#Gather ec2 key pairs and instance id's

keyPairs=$(aws ec2 describe-key-pairs --key-names "$CLUSTERID"Key | grep "KeyName" | awk '{print $2}' | sed -s 's/,/ /g' | tr -d '"')


#Gather S3 buckets
s3Buckets=$(aws s3api list-buckets | grep $CLUSTERID | awk '{print $2}' | tr -d '"')

#Gather IAM Users
iamUser=$(aws iam list-users --query "Users[?UserName=='$CLUSTERID-ACD' || UserName=='$CLUSTERID-singlecomm-net' || UserName=='$CLUSTERID.sngl.cm'].[UserName]" --output text)



echo ""
echo "The following resources were found:

Log groups:
$logGroups

Key pairs:
$keyPairs

S3 Buckets:
$s3Buckets

IAM Users:
$iamUser
"


if [[ $DELETE == 'false' ]];then
   exit 1
fi



