import boto3, argparse

def main():
    pass

def get_ips():
    
    # get all elastic ips
    snapshots = []

    snapshot = ec2_client.describe_snapshots()
    print(snapshot)


if __name__ == '__main__':

    # Defining which AWS account to run against
    parser = argparse.ArgumentParser(description="Gets snapshot ID's that are older than the current year")
    parser.add_argument("-p", "--profile", nargs="?", default=str(655), help="AWS account to target")
    args = parser.parse_args()

    # need to set the account id string per account
    if args.profile == "655":
        account_id = "655772981054"
    
    if args.profile == "740":
        account_id = "740862182994"

    # Creating the connection
    session = boto3.Session(profile_name=str(args.profile))
    ec2_client = session.client('ec2')
    print('Connected to ec2 for account {}'.format(args.profile))

    exit(get_ips())