import argparse, boto3

def main():

    snapshot_ids = []
    start_times = []

    # Create dict of that contains information about ALL snapshots in the account
    snapshots = [s for s in ec2_client.describe_snapshots(
        OwnerIds=[
            str(account_id)
        ]
    )['Snapshots']]

    # Append to the empty lists (snapshot_ids, start_times) when there's an instance without tags
    for snap in snapshots:
        try:
            tagged = snap['Tags']
        except KeyError:
            snapshot_ids.append(snap['SnapshotId'])
            start_times.append(snap['StartTime'])
    
    for indv_snap, indv_startime in zip(snapshot_ids, start_times):
        print('Start time for {} is: {}'.format(indv_snap, indv_startime))
    


if __name__ == '__main__':

    # Defining which AWS account to run against
    parser = argparse.ArgumentParser(description="Gets snapshot ID's that have no tags")
    parser.add_argument("-p", "--profile", nargs="?", default=str(655), help="AWS account to target")
    args = parser.parse_args()

    if args.profile == "655":
        account_id = "655772981054"
    elif args.profile == "740":
        account_id = "740862182994"

    session = boto3.Session(profile_name=str(args.profile))
    ec2_client = session.client('ec2')
    print('Connected to the {} account'.format(account_id))
    exit(main())