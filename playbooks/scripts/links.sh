#!/bin/bash

PREV_DATE="$(date +%Y-%m-%d -d yesterday)"

acdlinksSC=(
"argo"
"cuore"
"nexrep"
"synergixx"
"lt"
)
umg="umg"
cuore="cuore"

for name in "${acdlinksSC[@]}"; do
  echo "https://$name.singlecomm.com/admin/ipv6.sc?sdate=$PREV_DATE&date=$PREV_DATE"
done

echo "https://$umg.singlecomm.com/admin/ipv6.$umg?sdate=$PREV_DATE&date=$PREV_DATE"
