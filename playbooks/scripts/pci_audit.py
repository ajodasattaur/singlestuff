import boto3, json, sys, csv

# Create a session for the 655 account
session = boto3.Session(profile_name=str(655))
s3_session = session.resource('s3')

# Read the desired cloudtrail file
cloudtrail_file = s3_session.Object('sc-devops-backups', 'test/cloudtrail_events_655.json')
# Decode objects coming from utf-8
cloudtrail_contents = cloudtrail_file.get()['Body'].read().decode('utf-8')

# Create a json var with the contents from that file
json_content = json.loads(cloudtrail_contents)


# Create a list of all users that have had events
empty_username_list = []

for username in json_content['Events']:
    try:
        username = username
        empty_username_list.append(username)
    except TypeError:
        pass

# Create a list for all trusted IP's in trusted_ips.txt
trusted_ips_csv = []

with open("/home/ajodas/Log-Auditing-Tools/ip.txt", "r") as csvfile:
    reader = csv.reader(csvfile, delimiter=',')
    for row in reader:
        trusted_ips_csv.append(row)

trusted_ips = [ip[1] for ip in trusted_ips_csv]
print(trusted_ips)