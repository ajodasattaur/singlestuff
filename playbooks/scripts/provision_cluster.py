# Begin imports
from troposphere import (
    Ref,
    ec2,
    Tags,
    Template,
    Parameter,
    Output,
    Join,
    Sub,
    route53,
    s3,
    Export
)
import argparse


# Begin constants

STAGE = "dev"

CIDR = "10.0.0.0/16"

REGION = "us-east-1"

PUBLIC_SUBNETS = [

    ("USEast1a", "us-east-1a", "10.20.20.0/24"),
    ("USEast1b", "us-east-1b", "10.20.30.0/24"),
    ("USEast1c", "us-east-1c", "10.20.40.0/24")

]

HA_NAT = True

#   Create an empty template
template = Template()

#   Begin parameters

#   Size of the instance to spin up
template.add_parameter(Parameter(
    "InstanceSize",
    Type="String",
    Default="c4.large",
    Description="Size of the instance you want to spin up"
))

#   Begin outputs

template.add_output(Output(
    "VPCID",
    Value=Ref("VPC"),
    Description="VPC ID",
    Export=Export(Join("-", [Ref("AWS::StackName"), "id"]))
))

for PUBLIC_SUBNET in PUBLIC_SUBNETS:

    template.add_resource(ec2.Subnet(
        "PublicSubnet%s" % (PUBLIC_SUBNET[0]),
        VpcId=Ref("VPC"),
        CidrBlock="%s" % (PUBLIC_SUBNET[2]),
        AvailabilityZone="%s" % (PUBLIC_SUBNET[1]),
        MapPublicIpOnLaunch=True,
        Tags=Tags(
            Stage="%s" % (STAGE),
            Name="%s-vpc-subnet-public-%s" % (STAGE, PUBLIC_SUBNET[1]),
            Region="%s" % (REGION)
        )
    ))

    if HA_NAT:
        
        template.add_resource(ec2.EIP(
            "PrivateNatEIP%s" % (PUBLIC_SUBNET[0]),
            Domain="vpc"
        ))




print(template.to_yaml())

# Start arguments
parser = argparse.ArgumentParser(description="Spin up a cluster of resources that make up a FreeSWITCH platform")
parser.add_argument("-s", "--size", help="Size of the cluster you want to spin up")
parser.add_argument("-p", "--profile", default=str("655"), nargs="?", help="AWS profile that you want to spin up the cluster in")
args = parser.parse_args()
