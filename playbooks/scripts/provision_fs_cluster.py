from troposphere import (
    Ref,
    ec2,
    Tags,
    Template,
    Parameter,
    Output,
    Join,
    Sub,
    route53,
    s3
)

# Amount of FreeSWITCHES to place in the cluster
CLUSTER_SIZE = 5

# ! Create the empty template
template = Template()

# ! Begin parameters for template (subnet, AZ)

# ! Set client name
template.add_parameter(Parameter(
    "ClientName",
    Description="Name of the client/cluster i.e argo-fs",
    Type="String"
))

# Create subnet
template.add_parameter(Parameter(
    "ClientSubnet",
    Description="Subnet to use for client",
    Type="String"
))

# Add AZ
template.add_parameter(Parameter(
    "ClientAZ",
    Description="AZ to put the subnet in",
    Type="String",
    Default="us-east-1a",
    AllowedValues=[

        "us-east-1a",
        "us-east-1b",
        "us-east-1c",
        "us-east-1d"
    ]
))

# Set instance size
template.add_parameter(Parameter(
    "InstanceSize",
    Type="String",
    Default="c4.large",
    Description="Size of the instances to provision"
))


# Create dev VPC

template.add_resource(ec2.SecurityGroup(
    "devfs",
    GroupName="dev-fs",
    GroupDescription="Development SG for FreeSWITCH",
    SecurityGroupEgress=[
        ec2.SecurityGroupRule(
            IpProtocol="ALL",
            FromPort="0",
            ToPort="65535"
        )
    ],
    SecurityGroupIngress=[
        ec2.SecurityGroupRule(
            IpProtocol="tcp",
            FromPort="5066",
            ToPort="5067",
            CidrIp="0.0.0.0/0",
            Description="IB port for FreeSWITCH"
        ),
        ec2.SecurityGroupRule(
            IpProtocol="tcp",
            FromPort="5080",
            ToPort="5080",
            CidrIp="0.0.0.0/0"
        ),
        ec2.SecurityGroupRule(
            IpProtocol="tcp",
            FromPort="8088",
            ToPort="8088",
            CidrIp="0.0.0.0/0",
            Description="Port that plivo connects to on"
        ),
        ec2.SecurityGroupRule(
            IpProtocol="tcp",
            FromPort="5050",
            ToPort="5050",
            CidrIp="0.0.0.0/0",
        ),
        ec2.SecurityGroupRule(
            IpProtocol="tcp",
            FromPort="22",
            ToPort="22",
            CidrIp="0.0.0.0/0",
            Description="Standard SSH"
        ),
        ec2.SecurityGroupRule(
            IpProtocol="tcp",
            FromPort="2222",
            ToPort="2222",
            CidrIp="0.0.0.0/0",
            Description="Non standard SSH"
        )
    ],
))


# Iterate of cluster size and create an instance for each
# Creates an instance with a Name tag of $clientname-fs$XX i.e argo-fs01, argo-fs02

for instance in range(1, CLUSTER_SIZE + 1):
    i = str(instance).zfill(2)
    template.add_resource(ec2.Instance(
        "{}".format(i),
        InstanceType=Ref("InstanceSize"),
        ImageId="ami-855c7ee0",
        SubnetId=Ref("ClientSubnet"),
        Tags=Tags(
            Name=Sub("ClientName%s" % (i)) 
        )
    )),
    template.add_resource(route53.RecordSetType(
        "argo{}".format(i),
        Name=Ref("ClientName"),
        Type="A",
        HostedZoneId="xxxxxxxx"
    ))

template.add_resource(s3.Bucket(
    "Argofs",
    AccessControl="PublicReadWrite",
    BucketName=Ref("ClientName")
))

print(template.to_yaml())
        # "{}".format(i),