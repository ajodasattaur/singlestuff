import sys
import re
import datetime
from lxml import etree
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait as wait

cluster=sys.argv[1];
callid=sys.argv[2];

usernameStr = 'ajoda_sattaur';
passwordStr = 'Z5-tyvamor@abcdefg';

homeruser = 'ajoda';
homerpw = 'Z5-tyvamor';

browser = webdriver.Chrome()
browser.get(('https://' + cluster + '.singlecomm.com/'));

# fill in username and hit the next button

username = browser.find_element_by_name('email');
username.send_keys(usernameStr);
password = browser.find_element_by_name( "password" );
password.send_keys(passwordStr);

submit = browser.find_element_by_name( "submit" );
submit.click()


browser.get('https://' + cluster + '.singlecomm.com/supervisor/call.sc?id=' + callid + '#call_');

date=browser.find_element_by_xpath('//*[@id="section_details"]/div[2]/div[2]');
datefrom = date.get_attribute('innerHTML').strip();

date=browser.find_element_by_xpath('//*[@id="section_details"]/div[4]/div[2]');
dateto = date.get_attribute('innerHTML').strip();

datefrom = datetime.datetime.strptime(datefrom, '%m/%d/%Y %H:%M:%S').strftime('%Y-%m-%d %H:%M:%S');
dateto = datefrom;

print "from: " + datefrom;
print "to  : " + dateto;

### switch to CDR tab

browser.get('https://' + cluster + '.singlecomm.com/supervisor/call.sc?id=' + callid + '#cdr_');

### Click on CDR tab
url=browser.find_element_by_xpath('//*[@id="updatecall_'+callid+'"]/ul/li[6]/a');
url.click();

### click on the CDR link
url=browser.find_element_by_xpath('//*[@id="callCDR"]/tbody/tr[1]/td[1]/a');
url.click();

# Switch tab to the new tab, which we will assume is the next one on the right
browser.switch_to_window(browser.window_handles[1]);

source=browser.page_source;
m = re.search('<sip_call_id>(.*)</sip_call_id>',source);
if m:
    found = m.group(1);
    found = re.sub(r'%40', '@', found);
    print found;

    ### let's go to homer
    browser.get('https://singlecapture.com/#/login');
    browser.implicitly_wait(10);

    # Switch tab to the new tab, which we will assume is the next one on the right
    browser.switch_to_window(browser.window_handles[1]);

    #browser.switch_to.window("SingleCapture");

    #print browser.page_source;

    #handles = browser.window_handles
    #for ii, hh in enumerate(handles):
    #    browser.switch_to.window(hh)
    #    print 'window %s has title %s' % (ii, browser.title)

    try:
        element = WebDriverWait(browser, 20).until(
            EC.presence_of_element_located((By.ID, "username"))
        )
    finally:
        print "I couldn't find that ID";
        #browser.quit();

    username = browser.find_element_by_id('username');
    username.send_keys(homeruser);
    password = browser.find_element_by_id("password");
    password.send_keys(homerpw);

    #submit_button = browser.find_elements_by_xpath('//*[@id="login_button"]/span');
    wait(browser, 10).until(EC.element_to_be_clickable((By.XPATH, '//*[@id="login_button"]/span'))).click();

    wait(browser, 10).until(EC.element_to_be_clickable((By.XPATH, '//*[@id="main"]/div/div/div/div[2]/div/div[1]/div[4]/div[2]/adf-widget-content/div/div[3]/ng-form/div/div/div/input'))).click();
    browser.get('https://singlecapture.com/#/dashboard/search');
    wait(browser, 10).until(EC.element_to_be_clickable((By.XPATH,'//*[@id="main"]/div/div/div/div[2]/div/div[1]/div[1]/div[2]/adf-widget-content/div/div[4]/ng-form/div/div/div/input'))).click();

    ######### set FROM date #########

    browser.find_element_by_id('filterIndicator').click();
    datetime_selector = browser.find_element_by_id('datetimepickerFrom');
    datetime_selector.click();
    datetime_selector.clear();
    datetime_selector.send_keys(datefrom[:10]);

    hour = browser.find_element_by_xpath('//*[@id="timeselector"]/div/div[1]/p[2]/span/table/tbody/tr[2]/td[1]/input');
    hour.clear();
    hour.send_keys('00');

    minutes = browser.find_element_by_xpath('//*[@id="timeselector"]/div/div[1]/p[2]/span/table/tbody/tr[2]/td[3]/input');
    minutes.clear();
    minutes.send_keys('00');

    seconds = browser.find_element_by_xpath('//*[@id="timeselector"]/div/div[1]/p[2]/span/table/tbody/tr[2]/td[5]/input');
    seconds.clear();
    seconds.send_keys('00');

    ######### set TO date #########

    datetime_selector = browser.find_element_by_id('datetimepickerTo');
    datetime_selector.click();
    datetime_selector.clear();
    datetime_selector.send_keys(dateto[:10]);

    hour = browser.find_element_by_xpath('//*[@id="timeselector"]/div/div[1]/p[4]/span/table/tbody/tr[2]/td[1]/input');
    hour.clear();
    hour.send_keys('23');

    minutes = browser.find_element_by_xpath('//*[@id="timeselector"]/div/div[1]/p[4]/span/table/tbody/tr[2]/td[3]/input');
    minutes.clear();
    minutes.send_keys('59');

    seconds = browser.find_element_by_xpath('//*[@id="timeselector"]/div/div[1]/p[4]/span/table/tbody/tr[2]/td[5]/input');
    seconds.clear();
    seconds.send_keys('59');

    date_submit = browser.find_element_by_xpath('//*[@id="timeselector"]/div/div[1]/div/button');
    date_submit.click();

    # Set call id we're looking for
    callid = browser.find_element_by_xpath('//*[@id="main"]/div/div/div/div[2]/div/div[1]/div[1]/div[2]/adf-widget-content/div/div[4]/ng-form/div/div/div/input');
    callid.clear();
    callid.send_keys(found);
    submit = browser.find_element_by_xpath('//*[@id="main"]/div/div/div/div[2]/div/div[1]/div[1]/div[2]/adf-widget-content/div/div[5]/button[1]');
    submit.click();



else:
    print "not found";


