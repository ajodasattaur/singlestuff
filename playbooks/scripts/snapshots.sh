#!/bin/bash

SnapshotDate=$(date --date="-7 days" +%Y-%m-%d)

aws ec2 describe-snapshots --query 'Snapshots[?StartTime<="$SnapshotDate"]' --region 'us-east-1'