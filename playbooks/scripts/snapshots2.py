import boto3, argparse

def main():
    print(get_data())


def get_data():
    snapshot_list = []

    snapshots = [s for s in ec2_client.describe_snapshots(
        OwnerIds=[
            '655772981054'
        ]
    )["Snapshots"]]

    for snap in snapshots:

        try:

            tagged = snap["Tags"]

        except KeyError:

            snapshot_list.append(snap["SnapshotId"])

    return len(snapshot_list)

if __name__ == "__main__":

    # Defining which AWS account to run against
    parser = argparse.ArgumentParser(description="Gets snapshot ID's that are older than the current year")
    parser.add_argument("-p", "--profile", nargs="?", default=str(655), help="AWS account to target")
    args = parser.parse_args()

    # Creating the connection
    session = boto3.Session(profile_name=str(args.profile))
    ec2_client = session.client('ec2')
    print('Connected to ec2 for account {}'.format(args.profile))
    exit(main())