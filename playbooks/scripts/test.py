import boto3
import datetime
import json

ec2_client = boto3.client('ec2')
def main():
    volume_id = get_volumes()
    volume_exist(volume_id)

def get_volumes():

    volume_id = []  
    """
    Get's all the current snapshots in the 655 account

    Returns:
      Array that has all the snapshot data inside
    """
    snapshots = ec2_client.describe_snapshots()
    
    # Strip each volume ID from the corresponding snapshot ID and appends them to the snapshot_id array
    for snapshot in snapshots["Snapshots"]:
        volume_id.append(snapshot["VolumeId"])
    return volume_id

def volume_exist(volume_id):
    volume = ec2_client.describe_volumes(VolumeIds=[volume_id])
    for key, value in volume:
        print(key, value)


if __name__ == "__main__":
    exit(main())