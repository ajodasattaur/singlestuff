#!/bin/bash

check_process=(
- "ntpd"
)

for name in "${check_process[@]}"; do
  echo -n "$name: "
  pgrep $name > dev/null && echo "running" || echo "not running"
done
