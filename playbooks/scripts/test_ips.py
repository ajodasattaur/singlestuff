import boto3,argparse

# def compare_ip():
#     non_working_ip_addresses = [line.rstrip('\n') for line in open('ips.txt')]
#     active_ips = []
#     for ip in ip_addresses:


def main():
    print(get_public_ips())


def read_ips():
    non_working_ip_addresses = [line.rstrip('\n') for line in open('ips.txt')]
    for ip in non_working_ip_addresses:
        print(ip)

def get_public_ips():
    account_ips = []
    reservations = ec2_client.describe_instances()

    for reservation in reservations["Reservations"]:
        for instance in reservation["Instances"]:
            try:
                account_ips.append(instance["PublicIpAddress"])

            except KeyError:
                pass

    return account_ips

    
# If this is run interactively, provide the profile to use
if __name__ == "__main__":

    parser = argparse.ArgumentParser(description='Gets all public ips of ec2 instances in an AWS account')
    parser.add_argument("-p", "--profile", nargs="?", help="AWS account you want to target")
    args = parser.parse_args()

    session = boto3.Session(profile_name=str(args.profile))
    ec2_client = session.client('ec2')

# Invoke
    exit(main())