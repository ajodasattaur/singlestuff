import troposphere

# Create template
template = troposphere.Template()


# Create a test VPC
test_vpc = troposphere.ec2.VPC(
    "ajodaTest",
    CidrBlock="10.0.0.0/16",
    EnableDnsSupport=True,
    EnableDnsHostnames=True,
    Tags=Tags(
        Name="ajodatest"
    ))
template.add_resource(test_vpc)
print(template.to_yaml())