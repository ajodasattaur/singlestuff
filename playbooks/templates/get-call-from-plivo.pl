#!/usr/bin/perl
use strict;
use Data::Dumper;

my ($cldnum, $filename) = @ARGV;
 
if (not defined $cldnum) {
  die "Need: $0 [CLDNUM] [FILENAME]\n\nWhere:\n\tCLDNUM is the number to look for.\n\tFILENAME is the log file in which to look\n";
}

if (not defined $filename) {
    $filename = "/usr/local/plivo/tmp/plivo-rest.log.1";
}

my $ruuid = "";
my $found = 0;
my $datetime = "";
my %log;
my $results = 0;

open(FILE,"<",$filename); 

while(my $data = <FILE>){ 
	if( $data =~ /$cldnum/ ){ 
	    if( $results > 100 ){ die "Too many results! ($results)\n"; } else {  $results++; }
	    ($datetime) = $data =~ /^(20[0-9][0-9]-[0-9][0-9]-[0-9][0-9] [0-9][0-9]:[0-9][0-9]:[0-9][0-9],[0-9][0-9][0-9])/;
		$log{$datetime} .= $data;
		if( $data =~ /Fetching POST/) { 
			($ruuid) = $data =~ /RequestUUID': u\'(.*?)\',/;
			$found = 1;
		}
	}
}

if($found) {
    close(FILE);
    open(FILE,"<","/usr/local/plivo/tmp/plivo-rest.log.1");
    while(my $data = <FILE>){ 
        if( $data =~ /$ruuid/ ){ 
    	    if( $results > 100 ){ die "Too many results! ($results)\n"; } else {  $results++; }
            ($datetime) = $data =~ /^(20[0-9][0-9]-[0-9][0-9]-[0-9][0-9] [0-9][0-9]:[0-9][0-9]:[0-9][0-9],[0-9][0-9][0-9])/;
            $log{$datetime} .= $data;
        }
    }
}

foreach (sort keys %log) {
    print "$_ : $log{$_}\n";
};

close(FILE);
1;